package com.drinkinggame.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.drinkinggame.R;
import com.drinkinggame.dataBase.DrinkingGameLocalDatabaseSqlite;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private DrinkingGameLocalDatabaseSqlite databaseSqlite;
    private Button resumeGameButton;
    private Button gameModeButton;
    private String[] gameModes = new String[]{"Alles", "Normal", "Pervers", "Männerrunde"};
    private int currentMode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Buttons on Screen
        Button startGameButton = findViewById(R.id.newGameButton);
        resumeGameButton = findViewById(R.id.resumeGameButton);
        Button editGamersButton = findViewById(R.id.editPlayersButton);
        gameModeButton = findViewById(R.id.gameModeButton);
        Button rulesButton = findViewById(R.id.rulesButton);

        databaseSqlite = new DrinkingGameLocalDatabaseSqlite(this);

        if (databaseSqlite.readActiveQuestions() == null) {
            resumeGameButton.setBackgroundResource(R.drawable.rounded_grey_button);
            resumeGameButton.setTextColor(getResources().getColor(R.color.colorGrey));
        } else {
            resumeGameButton.setOnClickListener(this);
        }

        String currentModeText = "Modus:" + gameModes[currentMode];
        gameModeButton.setText(currentModeText);

        startGameButton.setOnClickListener(this);
        editGamersButton.setOnClickListener(this);
        gameModeButton.setOnClickListener(this);
        rulesButton.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        if (databaseSqlite.readActiveQuestions() == null) {
            resumeGameButton.setBackgroundResource(R.drawable.rounded_grey_button);
            resumeGameButton.setTextColor(getResources().getColor(R.color.colorGrey));
        } else {
            resumeGameButton.setBackgroundResource(R.drawable.rounded_button);
            resumeGameButton.setTextColor(getResources().getColor(R.color.colorBlack));
            resumeGameButton.setOnClickListener(this);
        }


        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.newGameButton:
                Intent intent = new Intent(MainActivity.this, ActivityGame.class);
                intent.putExtra("currentMode", currentMode);
                startActivity(intent);
                break;
            case R.id.resumeGameButton:
                Intent intent2 = new Intent(MainActivity.this, ActivityGame.class);
                intent2.putExtra("resumeGame", true);
                startActivity(intent2);
                break;
            case R.id.editPlayersButton:
                Intent intent3 = new Intent(MainActivity.this, ActivityPlayers.class);
                startActivity(intent3);
                break;
            case R.id.gameModeButton:
                if (currentMode < gameModes.length - 1) {
                    currentMode++;
                } else {
                    currentMode = 0;
                }
                String currentModeText = "Modus:" + gameModes[currentMode];
                gameModeButton.setText(currentModeText);
                break;
            case R.id.rulesButton:
                Intent intent4 = new Intent(MainActivity.this, ActivityRules.class);
                startActivity(intent4);
                break;
        }
    }
}