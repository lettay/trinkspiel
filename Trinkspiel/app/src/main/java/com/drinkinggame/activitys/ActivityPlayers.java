package com.drinkinggame.activitys;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.drinkinggame.R;
import com.drinkinggame.dataBase.DrinkingGameLocalDatabaseSqlite;
import com.drinkinggame.dataBase.dataObjects.Player;
import com.drinkinggame.listAdapters.PlayerListAdapter;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityPlayers extends AppCompatActivity implements View.OnClickListener {
    DrinkingGameLocalDatabaseSqlite databaseSqlite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players);

        Button homeButton = findViewById(R.id.homeButton);
        homeButton.setOnClickListener(this);

        Button addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(this);

        databaseSqlite = new DrinkingGameLocalDatabaseSqlite(this);

        populateList();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.homeButton:
                super.onBackPressed();
                break;

            case R.id.addButton:
                EditText textInputEditText = findViewById(R.id.textInput);
                String name = textInputEditText.getText().toString();
                if (name.isEmpty()) {
                    Toast.makeText(this, "Bitte einen Namen Eingeben", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    databaseSqlite.insertPlayer(name);
                    textInputEditText.setText("", TextView.BufferType.EDITABLE);
                    textInputEditText.setHint(R.string.hint_content_title);
                    populateList();
                }
                break;
        }
    }

    protected void populateList() {
        Player[] players = databaseSqlite.readPlayers();
        if (players != null) {
            ArrayList<Player> listPlayer = new ArrayList<>(Arrays.asList(players));


            PlayerListAdapter adapter = new PlayerListAdapter(listPlayer, this);

            ListView mainListView = findViewById(R.id.mainList);
            mainListView.setAdapter(adapter);
        }
    }
}