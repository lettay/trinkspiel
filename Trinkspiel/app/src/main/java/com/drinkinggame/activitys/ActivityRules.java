package com.drinkinggame.activitys;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.drinkinggame.R;
import com.drinkinggame.dataBase.DrinkingGameLocalDatabaseSqlite;
import com.drinkinggame.dataBase.dataObjects.Question;
import com.drinkinggame.listAdapters.QuestionListAdapter;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityRules extends AppCompatActivity implements View.OnClickListener {
    DrinkingGameLocalDatabaseSqlite databaseSqlite;
    private boolean listView = false;
    private RelativeLayout interfaceRules;
    private ListView mainListRules;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);

        //backButton
        Button backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(this);


        //interfaceQuestions Buttons
        Button questionsButton = findViewById(R.id.questionsButton);
        questionsButton.setOnClickListener(this);

        Button playerQuestionsButton = findViewById(R.id.playerQuestionsButton);
        playerQuestionsButton.setOnClickListener(this);

        Button taskButton = findViewById(R.id.tasksButton);
        taskButton.setOnClickListener(this);

        Button ownRulesButton = findViewById(R.id.ownRulesButton);
        ownRulesButton.setOnClickListener(this);


        interfaceRules = findViewById(R.id.interfaceRules);
        mainListRules = findViewById(R.id.mainListRules);
        mainListRules.setVisibility(View.GONE);

        databaseSqlite = new DrinkingGameLocalDatabaseSqlite(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButton:
                if (!listView) {
                    super.onBackPressed();
                } else {
                    interfaceRules.setVisibility(View.VISIBLE);
                    mainListRules.setVisibility(View.GONE);
                    listView = false;
                }
                break;

            case R.id.questionsButton:
                interfaceRules.setVisibility(View.GONE);
                mainListRules.setVisibility(View.VISIBLE);
                populateList("question");
                listView = true;
                break;

            case R.id.playerQuestionsButton:
                interfaceRules.setVisibility(View.GONE);
                mainListRules.setVisibility(View.VISIBLE);
                populateList("questionPlayers");
                listView = true;
                break;

            case R.id.tasksButton:
                interfaceRules.setVisibility(View.GONE);
                mainListRules.setVisibility(View.VISIBLE);
                populateList("task");
                listView = true;
                break;

            case R.id.ownRulesButton:
                Toast.makeText(this, "Bald verfügbar!", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    protected void populateList(String type) {
        ArrayList<Question> questions = databaseSqlite.readQuestionsByType(type);

        if (questions != null) {
            QuestionListAdapter adapter = new QuestionListAdapter(questions, this);

            mainListRules.setAdapter(adapter);
        }
    }
}
