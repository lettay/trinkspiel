package com.drinkinggame.activitys;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.drinkinggame.R;
import com.drinkinggame.dataBase.DrinkingGameLocalDatabaseSqlite;
import com.drinkinggame.dataBase.dataObjects.Player;
import com.drinkinggame.dataBase.dataObjects.Question;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityGame extends AppCompatActivity implements View.OnClickListener {
    private String rightAnswer = "";
    private DrinkingGameLocalDatabaseSqlite databaseSqlite;
    private ArrayList<Question> questions;
    private TextView playerNameTextView;
    private TextView questionTextView;
    private TextView gameSizeTextView;
    private RelativeLayout questionsLayout;
    private Button answer1Button;
    private Button answer2Button;
    private Button answer3Button;
    private Button answer4Button;
    private Player[] players;
    private String type;
    private int gameSize = 0;
    private int currentPlayer = 0;
    private int currentQuestion = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        databaseSqlite = new DrinkingGameLocalDatabaseSqlite(this);

        //getting all active players
        players = databaseSqlite.readPlayers();

        if (players == null || players.length == 1) {
            Toast.makeText(this, "Ihr müsst mindestens 2 Spieler hinzufügen", Toast.LENGTH_SHORT).show();
            super.onBackPressed();
        } else {
            setContentView(R.layout.activity_game);

            Button homeButton = findViewById(R.id.homeButton);
            homeButton.setOnClickListener(this);


            questionTextView = findViewById(R.id.questionTextView);
            questionsLayout = findViewById(R.id.questionsLayout);
            //getting the Buttons
            answer1Button = findViewById(R.id.answer1Button);
            answer2Button = findViewById(R.id.answer2Button);
            answer3Button = findViewById(R.id.answer3Button);
            answer4Button = findViewById(R.id.answer4Button);

            playerNameTextView = findViewById(R.id.playerNameTextView);
            gameSizeTextView = findViewById(R.id.gameSizeTextView);

            boolean resumeGame = getIntent().getBooleanExtra("resumeGame", false);
            int currentMode = getIntent().getIntExtra("currentMode", 0);

            if (!resumeGame) {

                //getting all the questions
                if (currentMode == 0) {
                    questions = databaseSqlite.readQuestions();
                } else {
                    questions = databaseSqlite.readQuestionsByMode(currentMode);
                }

                //setting the current active player to the first one
                gameSize = questions.size();
                //setting the initial gamesize
                databaseSqlite.insertActiveGameData(players[0].getPlayerId(), gameSize);

                //initialising an active game with the question/task set
                databaseSqlite.insertActiveGame(questions);
            } else {
                //loading all active questions(questions which are not done allready in the previous game)
                questions = databaseSqlite.readActiveQuestions();

                //reloading the gamesize out of the database
                gameSize = databaseSqlite.readGameSize();

                //loading the playerId of the current player, who's turn it is
                int currentPlayerId = databaseSqlite.readActivePlayerId();

                //looping through all players to match the playerId
                for (int i = 0; i < players.length; i++) {
                    if (currentPlayerId == players[i].getPlayerId()) {
                        //setting the current player to the position of the matching playerId in the array
                        currentPlayer = i;
                        //ending the loop
                        i = players.length;
                    }
                }
            }

            //setting the name of the current Player
            String playerName = players[currentPlayer].getName() + ":";
            playerNameTextView.setText(playerName);

            //starting first round
            nextRound();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.homeButton:
                super.onBackPressed();
                break;

            case R.id.answer1Button:
            case R.id.answer2Button:
            case R.id.answer3Button:
            case R.id.answer4Button:
                Button clicked = findViewById(v.getId());

                if (type.equals("question")) {
                    if (rightAnswer.equals(clicked.getText().toString())) {
                        clicked.setBackgroundResource(R.drawable.rounded_button_right);

                        //setting the Right Task
                        questionTextView.setText(questions.get(currentQuestion).getTaskRight());
                        questionTextView.setTextColor(getResources().getColor(R.color.colorGreen));
                    } else {
                        clicked.setBackgroundResource(R.drawable.rounded_button_wrong);

                        //setting the Wrong Task
                        questionTextView.setText(questions.get(currentQuestion).getTaskWrong());
                        questionTextView.setTextColor(getResources().getColor(R.color.colorRed));
                    }
                } else if (type.equals("questionPlayers")) {
                    String taskText = questions.get(currentQuestion).getTaskRight() + clicked.getText() + questions.get(currentQuestion).getTaskWrong();
                    questionTextView.setText(taskText);
                    clicked.setBackgroundResource(R.drawable.rounded_button_right);
                }

                //listening on TextField Clicks
                questionTextView.setOnClickListener(this);

                //disabling the answer buttons
                answer1Button.setOnClickListener(null);
                answer2Button.setOnClickListener(null);
                answer3Button.setOnClickListener(null);
                answer4Button.setOnClickListener(null);

                break;
            case R.id.questionTextView:
            case R.id.questionsLayout:
                //setting the colors of the buttons back
                answer1Button.setBackgroundResource(R.drawable.rounded_button);
                answer2Button.setBackgroundResource(R.drawable.rounded_button);
                answer3Button.setBackgroundResource(R.drawable.rounded_button);
                answer4Button.setBackgroundResource(R.drawable.rounded_button);
                //setting the Text Color back to white
                questionTextView.setTextColor(getResources().getColor(R.color.colorWhite));
                //disabling the onClick Listeners for Questions and Text Field
                questionTextView.setOnClickListener(null);

                currentPlayer++;
                //setting the Name of the Next Players turn
                if (currentPlayer < players.length) {
                    String playerName = players[currentPlayer].getName() + ":";
                    playerNameTextView.setText(playerName);
                } else {
                    currentPlayer = 0;
                    String playerName = players[currentPlayer].getName() + ":";
                    playerNameTextView.setText(playerName);
                }

                //removing the question from the current set
                databaseSqlite.removeActiveQuestion(questions.get(currentQuestion).getQuestionId());
                questions.remove(currentQuestion);

                //changing the active player(turn of the player)
                databaseSqlite.insertActiveGameData(players[currentPlayer].getPlayerId(), gameSize);

                //currentQuestion = (int) (Math.random() * ((questions.size()) + 1));
                //System.out.println(currentQuestion);

                //starting next round
                nextRound();
                break;
        }
    }

    private void nextRound() {
        if (questions != null && !questions.isEmpty()) {
            type = questions.get(currentQuestion).getType();

            //setting the top right overview of the current round
            int currentRound = gameSize - questions.size() + 1;
            String gameSizeText = "(" + currentRound + "/" + gameSize + ")";
            gameSizeTextView.setText(gameSizeText);

            switch (type) {
                case "task":
                    questionsLayout.setVisibility(View.GONE);
                    questionTextView.setText(questions.get(currentQuestion).getQuestion());
                    questionTextView.setOnClickListener(this);
                    break;
                case "question":
                    questionsLayout.setVisibility(View.VISIBLE);
                    //setting the right answer
                    rightAnswer = questions.get(currentQuestion).getRight();

                    //getting a array with all answers
                    String[] answers = new String[]{questions.get(currentQuestion).getFalse1(), questions.get(currentQuestion).getFalse2(), questions.get(currentQuestion).getFalse3(), questions.get(currentQuestion).getRight()};

                    //converting the array in a List so it can be shuffled
                    List<String> answersList = Arrays.asList(answers);
                    Collections.shuffle(answersList);

                    //setting the text of the TextView and Buttons
                    questionTextView.setText(questions.get(currentQuestion).getQuestion());
                    answer1Button.setText(answersList.get(0));
                    answer2Button.setText(answersList.get(1));
                    answer3Button.setText(answersList.get(2));
                    answer4Button.setText(answersList.get(3));

                    //setting the click Listeners
                    answer1Button.setOnClickListener(this);
                    answer2Button.setOnClickListener(this);
                    answer3Button.setOnClickListener(this);
                    answer4Button.setOnClickListener(this);
                    break;
                case "questionPlayers":
                    questionsLayout.setVisibility(View.VISIBLE);

                    //setting the text of the TextView and Buttons
                    questionTextView.setText(questions.get(currentQuestion).getQuestion());


                    //getting all players as a List
                    List<Player> playersList = new LinkedList<>(Arrays.asList(players));
                    //removing the current player so only the other players are in it
                    playersList.remove(currentPlayer);
                    Collections.shuffle(playersList);

                    //there are min 2 player so there is at least 1 other player
                    answer1Button.setText(playersList.get(0).getName());
                    answer1Button.setOnClickListener(this);

                    //setting the Button empty in case there is not enough players
                    answer2Button.setText("");
                    answer3Button.setText("");
                    answer4Button.setText("");

                    int numberOtherPlayers = playersList.size();


                    if (numberOtherPlayers > 1) {
                        answer2Button.setText(playersList.get(1).getName());
                        answer2Button.setOnClickListener(this);

                        if (numberOtherPlayers > 2) {
                            answer3Button.setText(playersList.get(2).getName());
                            answer3Button.setOnClickListener(this);

                            if (numberOtherPlayers > 3) {
                                answer4Button.setText(playersList.get(3).getName());
                                answer4Button.setOnClickListener(this);
                            }
                        }
                    }
                    break;
            }
        } else {
            Toast.makeText(this, "Du hast alle Fragen durch!", Toast.LENGTH_LONG).show();
            super.onBackPressed();
        }
    }
}