package com.drinkinggame.dataBase.dataObjects;

public class Question {
    private int questionId;
    private int active;
    private String type;
    private String question;
    private String false1;
    private String false2;
    private String false3;
    private String right;
    private String taskRight;
    private String taskWrong;

    public Question(int questionId, int active, String type, String question, String false1, String false2, String false3, String right, String taskRight, String taskWrong) {
        this.questionId = questionId;
        this.active = active;
        this.type = type;
        this.question = question;
        this.false1 = false1;
        this.false2 = false2;
        this.false3 = false3;
        this.right = right;
        this.taskRight = taskRight;
        this.taskWrong = taskWrong;
    }

    public int getQuestionId() {
        return questionId;
    }

    public int getActive() {
        return active;
    }

    public String getType() {
        return type;
    }

    public String getQuestion() {
        return question;
    }

    public String getFalse1() {
        return false1;
    }

    public String getFalse2() {
        return false2;
    }

    public String getFalse3() {
        return false3;
    }

    public String getRight() {
        return right;
    }

    public String getTaskRight() {
        return taskRight;
    }

    public String getTaskWrong() {
        return taskWrong;
    }

    public void setActive(int active){
        this.active = active;
    }
}
