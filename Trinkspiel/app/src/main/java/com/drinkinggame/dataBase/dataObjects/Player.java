package com.drinkinggame.dataBase.dataObjects;

public class Player {
    private int playerId;
    private String name;
    private boolean active;

    private int orderNumber;

    public Player(int playerId, String name, boolean active, int orderNumber){
        this.playerId = playerId;
        this.name = name;
        this.active = active;
        this.orderNumber = orderNumber;
    }

    public int getPlayerId() {
        return playerId;
    }

    public String getName() {
        return name;
    }

    public boolean isActive() {
        return active;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }
}
