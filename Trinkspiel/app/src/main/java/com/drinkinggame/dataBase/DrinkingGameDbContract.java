package com.drinkinggame.dataBase;

import android.provider.BaseColumns;

/**
 * Class storing the table layout for a history entry.
 */
public class DrinkingGameDbContract {

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public DrinkingGameDbContract() {
    }

    abstract class QuestionEntry implements BaseColumns {
        static final String TABLE_NAME_QUESTION = "questionTable";
        static final String COLUMN_NAME_TYPE = "type";
        static final String COLUMN_NAME_MODE = "mode";
        static final String COLUMN_NAME_ACTIVE = "active";
        static final String COLUMN_NAME_ASKED = "asked";
        static final String COLUMN_NAME_QUESTION = "question";
        static final String COLUMN_NAME_FALSE1 = "false1";
        static final String COLUMN_NAME_FALSE2 = "false2";
        static final String COLUMN_NAME_FALSE3 = "false3";
        static final String COLUMN_NAME_RIGHTANSWER = "rightAnswer";
        static final String COLUMN_NAME_TASKRIGHT = "taskRight";
        static final String COLUMN_NAME_TASKWRONG = "taskWrong";
    }

    abstract class ActiveGameEntry implements BaseColumns {
        static final String TABLE_NAME_ACTIVEGAME = "activeGameTable";
        static final String COLUMN_NAME_QUESTIONID = "questionId";
        static final String COLUMN_NAME_TYPE = "type";
        static final String COLUMN_NAME_QUESTION = "question";
        static final String COLUMN_NAME_FALSE1 = "false1";
        static final String COLUMN_NAME_FALSE2 = "false2";
        static final String COLUMN_NAME_FALSE3 = "false3";
        static final String COLUMN_NAME_RIGHTANSWER = "rightAnswer";
        static final String COLUMN_NAME_TASKRIGHT = "taskRight";
        static final String COLUMN_NAME_TASKWRONG = "taskWrong";
    }

    abstract class PlayerEntry implements BaseColumns {
        static final String TABLE_NAME_PLAYER = "playerTable";
        static final String COLUMN_NAME_NAME = "name";
        static final String COLUMN_NAME_ACTIVE = "active";
        static final String COLUMN_NAME_ORDERNUMBER = "orderNumber";
    }

    abstract class ActiveGameDataEntry implements BaseColumns {
        static final String TABLE_NAME_ACTIVEGAMEDATA = "activeGameDataTable";
        static final String COLUMN_NAME_PLAYERID = "playerID";
        static final String COLUMN_NAME_GAMESIZE = "gameSize";
    }

}
