package com.drinkinggame.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.drinkinggame.dataBase.DrinkingGameDbContract.ActiveGameDataEntry;
import com.drinkinggame.dataBase.DrinkingGameDbContract.ActiveGameEntry;
import com.drinkinggame.dataBase.DrinkingGameDbContract.PlayerEntry;
import com.drinkinggame.dataBase.DrinkingGameDbContract.QuestionEntry;
import com.drinkinggame.dataBase.dataObjects.Player;
import com.drinkinggame.dataBase.dataObjects.Question;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Database handler class for storing all Objects
 */
public class DrinkingGameLocalDatabaseSqlite extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "DrinkingGameDatabase.sqlite";

    public DrinkingGameLocalDatabaseSqlite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static final String SQL_CREATE_QUESTION_TABLE = "CREATE TABLE " + QuestionEntry.TABLE_NAME_QUESTION + " (" + QuestionEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + QuestionEntry.COLUMN_NAME_TYPE + ", " + QuestionEntry.COLUMN_NAME_MODE + " INTEGER, " + QuestionEntry.COLUMN_NAME_ACTIVE + " INTEGER, " + QuestionEntry.COLUMN_NAME_ASKED + " INTEGER, " + QuestionEntry.COLUMN_NAME_QUESTION + ", " + QuestionEntry.COLUMN_NAME_FALSE1 + ", " + QuestionEntry.COLUMN_NAME_FALSE2 + ", " + QuestionEntry.COLUMN_NAME_FALSE3 + ", " + QuestionEntry.COLUMN_NAME_RIGHTANSWER + ", " + QuestionEntry.COLUMN_NAME_TASKRIGHT + ", " + QuestionEntry.COLUMN_NAME_TASKWRONG + ");";

    private static final String SQL_CREATE_ACTIVEGAME_TABLE = "CREATE TABLE " + ActiveGameEntry.TABLE_NAME_ACTIVEGAME + " (" + ActiveGameEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + ActiveGameEntry.COLUMN_NAME_QUESTIONID + " INTEGER," + ActiveGameEntry.COLUMN_NAME_TYPE + ", " + ActiveGameEntry.COLUMN_NAME_QUESTION + ", " + ActiveGameEntry.COLUMN_NAME_FALSE1 + ", " + ActiveGameEntry.COLUMN_NAME_FALSE2 + ", " + ActiveGameEntry.COLUMN_NAME_FALSE3 + ", " + ActiveGameEntry.COLUMN_NAME_RIGHTANSWER + ", " + ActiveGameEntry.COLUMN_NAME_TASKRIGHT + ", " + ActiveGameEntry.COLUMN_NAME_TASKWRONG + ");";

    private static final String SQL_CREATE_PLAYER_TABLE = "CREATE TABLE IF NOT EXISTS " + PlayerEntry.TABLE_NAME_PLAYER + " (" + PlayerEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + PlayerEntry.COLUMN_NAME_NAME + ", " + PlayerEntry.COLUMN_NAME_ACTIVE + " INTEGER, " + PlayerEntry.COLUMN_NAME_ORDERNUMBER + " INTEGER);";

    private static final String SQL_CREATE_ACTIVEGAMEDATA_TABLE = "CREATE TABLE " + ActiveGameDataEntry.TABLE_NAME_ACTIVEGAMEDATA + " (" + ActiveGameDataEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + ActiveGameDataEntry.COLUMN_NAME_PLAYERID + " INTEGER, " + ActiveGameDataEntry.COLUMN_NAME_GAMESIZE + " INTEGER);";


    private static final String SQL_DELETE_ENTRIES_QUESTIONS = "DROP TABLE IF EXISTS " + QuestionEntry.TABLE_NAME_QUESTION + ";";
    private static final String SQL_DELETE_ENTRIES_ACTIVEGAME = "DROP TABLE IF EXISTS " + ActiveGameEntry.TABLE_NAME_ACTIVEGAME + ";";
    //private static final String SQL_DELETE_ENTRIES_PLAYER = "DROP TABLE IF EXISTS " + PlayerEntry.TABLE_NAME_PLAYER + ";";
    private static final String SQL_DELETE_ENTRIES_ACTIVEPLAYER = "DROP TABLE IF EXISTS " + ActiveGameDataEntry.TABLE_NAME_ACTIVEGAMEDATA + ";";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_QUESTION_TABLE);
        db.execSQL(SQL_CREATE_ACTIVEGAME_TABLE);
        db.execSQL(SQL_CREATE_PLAYER_TABLE);
        db.execSQL(SQL_CREATE_ACTIVEGAMEDATA_TABLE);

        initialiseQuestions(db);
        initialiseQuestionsPerverted(db);
        initialiseQuestionsGuysRound(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Code performing database upgrades, here just recreation
        db.execSQL(SQL_DELETE_ENTRIES_QUESTIONS);
        db.execSQL(SQL_DELETE_ENTRIES_ACTIVEGAME);
        db.execSQL(SQL_DELETE_ENTRIES_ACTIVEPLAYER);

        onCreate(db);
    }

    public void insertActiveGame(ArrayList<Question> questions) {
        SQLiteDatabase db = this.getWritableDatabase();

        //clearing the database
        db.execSQL("delete from " + ActiveGameEntry.TABLE_NAME_ACTIVEGAME);

        //inserting the passed question set
        for (int i = 0; i < questions.size(); i++) {
            //inserting the values
            ContentValues values = new ContentValues();
            values.put(ActiveGameEntry.COLUMN_NAME_QUESTIONID, questions.get(i).getQuestionId());
            values.put(ActiveGameEntry.COLUMN_NAME_TYPE, questions.get(i).getType());
            values.put(ActiveGameEntry.COLUMN_NAME_QUESTION, questions.get(i).getQuestion());
            values.put(ActiveGameEntry.COLUMN_NAME_FALSE1, questions.get(i).getFalse1());
            values.put(ActiveGameEntry.COLUMN_NAME_FALSE2, questions.get(i).getFalse2());
            values.put(ActiveGameEntry.COLUMN_NAME_FALSE3, questions.get(i).getFalse3());
            values.put(ActiveGameEntry.COLUMN_NAME_RIGHTANSWER, questions.get(i).getRight());
            values.put(ActiveGameEntry.COLUMN_NAME_TASKRIGHT, questions.get(i).getTaskRight());
            values.put(ActiveGameEntry.COLUMN_NAME_TASKWRONG, questions.get(i).getTaskWrong());

            db.insert(ActiveGameEntry.TABLE_NAME_ACTIVEGAME, "", values);
        }
    }

    public void insertPlayer(String name) {
        SQLiteDatabase db = this.getWritableDatabase();

        //getting the number of Players allready in the system
        String[] projection = {PlayerEntry.COLUMN_NAME_ORDERNUMBER};
        Cursor cursor = db.query(PlayerEntry.TABLE_NAME_PLAYER, projection, null, null, null, null, PlayerEntry.COLUMN_NAME_ORDERNUMBER);

        int orderNumberOfPlayer = 0;
        if (cursor.getCount() != 0) {
            cursor.moveToLast();
            orderNumberOfPlayer = cursor.getInt(cursor.getColumnIndex(PlayerEntry.COLUMN_NAME_ORDERNUMBER));
            orderNumberOfPlayer++;
        }
        cursor.close();

        //inserting the values
        ContentValues values = new ContentValues();
        values.put(PlayerEntry.COLUMN_NAME_NAME, name);
        values.put(PlayerEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(PlayerEntry.COLUMN_NAME_ORDERNUMBER, orderNumberOfPlayer);

        db.insert(PlayerEntry.TABLE_NAME_PLAYER, "", values);
    }

    public void insertActiveGameData(int playerId, int gameSize) {
        SQLiteDatabase db = this.getWritableDatabase();

        //clearing the database
        db.execSQL("delete from " + ActiveGameDataEntry.TABLE_NAME_ACTIVEGAMEDATA);

        //inserting the values
        ContentValues values = new ContentValues();
        values.put(ActiveGameDataEntry.COLUMN_NAME_PLAYERID, playerId);
        values.put(ActiveGameDataEntry.COLUMN_NAME_GAMESIZE, gameSize);

        db.insert(ActiveGameDataEntry.TABLE_NAME_ACTIVEGAMEDATA, "", values);
    }

    /**
     * @return the Question from the Database
     */
    public ArrayList<Question> readQuestions() {
        //only getting active Questions
        String selection = QuestionEntry.COLUMN_NAME_ACTIVE + " = ?";

        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(1)};

        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {QuestionEntry._ID, QuestionEntry.COLUMN_NAME_TYPE, QuestionEntry.COLUMN_NAME_QUESTION, QuestionEntry.COLUMN_NAME_FALSE1, QuestionEntry.COLUMN_NAME_FALSE2, QuestionEntry.COLUMN_NAME_FALSE3, QuestionEntry.COLUMN_NAME_RIGHTANSWER, QuestionEntry.COLUMN_NAME_TASKRIGHT, QuestionEntry.COLUMN_NAME_TASKWRONG};
        Cursor cursor = db.query(QuestionEntry.TABLE_NAME_QUESTION, // The table to query
                projection, // The columns to return
                selection, // The columns for the WHERE clause
                selectionArgs, // The values for the WHERE clause
                null, // don't group the rows
                null, // don't filter by row groups
                null // No sorting required for only one entry
        );
        cursor.moveToFirst();
        int cursorLength = cursor.getCount();
        //in case there are no Players
        if (cursorLength == 0) {
            return null;
        }

        cursor.moveToFirst();

        ArrayList<Question> questions = new ArrayList<>();

        for (int i = 0; i < cursorLength; i++) {
            int questionId = cursor.getInt(cursor.getColumnIndex(QuestionEntry._ID));
            String type = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TYPE));
            String question = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_QUESTION));
            String false1 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE1));
            String false2 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE2));
            String false3 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE3));
            String right = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_RIGHTANSWER));
            String taskRight = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TASKRIGHT));
            String taskWrong = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TASKWRONG));

            questions.add(new Question(questionId, 1, type, question, false1, false2, false3, right, taskRight, taskWrong));

            cursor.moveToNext();
        }

        //closing all connections
        cursor.close();
        db.close();

        Collections.shuffle(questions);
        Collections.shuffle(questions);

        return questions;
    }

    public ArrayList<Question> readQuestionsByMode(int currentMode) {
        //0=Standard Set 1=perverted 2=GuysRound
        currentMode--;
        // Define 'where' part of query
        String selection = QuestionEntry.COLUMN_NAME_MODE + " = ? AND " + QuestionEntry.COLUMN_NAME_ACTIVE + " = ? OR " + QuestionEntry.COLUMN_NAME_MODE + " = ? AND " + QuestionEntry.COLUMN_NAME_ACTIVE + " = ?";

        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(0), String.valueOf(1), String.valueOf(currentMode), String.valueOf(1)};

        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {QuestionEntry._ID, QuestionEntry.COLUMN_NAME_TYPE, QuestionEntry.COLUMN_NAME_QUESTION, QuestionEntry.COLUMN_NAME_FALSE1, QuestionEntry.COLUMN_NAME_FALSE2, QuestionEntry.COLUMN_NAME_FALSE3, QuestionEntry.COLUMN_NAME_RIGHTANSWER, QuestionEntry.COLUMN_NAME_TASKRIGHT, QuestionEntry.COLUMN_NAME_TASKWRONG};
        Cursor cursor = db.query(QuestionEntry.TABLE_NAME_QUESTION, // The table to query
                projection, // The columns to return
                selection, // The columns for the WHERE clause
                selectionArgs, // The values for the WHERE clause
                null, // don't group the rows
                null, // don't filter by row groups
                null // No sorting required for only one entry
        );

        cursor.moveToFirst();
        int cursorLength = cursor.getCount();
        //in case there are no Players
        if (cursorLength == 0) {
            return null;
        }

        cursor.moveToFirst();

        ArrayList<Question> questions = new ArrayList<>();

        for (int i = 0; i < cursorLength; i++) {
            int questionId = cursor.getInt(cursor.getColumnIndex(QuestionEntry._ID));
            String type = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TYPE));
            String question = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_QUESTION));
            String false1 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE1));
            String false2 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE2));
            String false3 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE3));
            String right = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_RIGHTANSWER));
            String taskRight = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TASKRIGHT));
            String taskWrong = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TASKWRONG));

            questions.add(new Question(questionId, 1, type, question, false1, false2, false3, right, taskRight, taskWrong));

            cursor.moveToNext();
        }

        //closing all connections
        cursor.close();
        db.close();

        Collections.shuffle(questions);
        Collections.shuffle(questions);

        return questions;
    }


    /**
     * @return the ActiveQuestion from the Database
     */
    public ArrayList<Question> readActiveQuestions() {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {ActiveGameEntry._ID, ActiveGameEntry.COLUMN_NAME_QUESTIONID, ActiveGameEntry.COLUMN_NAME_TYPE, ActiveGameEntry.COLUMN_NAME_QUESTION, ActiveGameEntry.COLUMN_NAME_FALSE1, ActiveGameEntry.COLUMN_NAME_FALSE2, ActiveGameEntry.COLUMN_NAME_FALSE3, ActiveGameEntry.COLUMN_NAME_RIGHTANSWER, ActiveGameEntry.COLUMN_NAME_TASKRIGHT, ActiveGameEntry.COLUMN_NAME_TASKWRONG};
        Cursor cursor = db.query(ActiveGameEntry.TABLE_NAME_ACTIVEGAME, // The table to query
                projection, // The columns to return
                null, // The columns for the WHERE clause
                null, // The values for the WHERE clause
                null, // don't group the rows
                null, // don't filter by row groups
                ActiveGameEntry._ID
        );
        cursor.moveToFirst();
        int cursorLength = cursor.getCount();
        //in case there are no Players
        if (cursorLength == 0) {
            return null;
        }

        cursor.moveToFirst();

        ArrayList<Question> questions = new ArrayList<>();

        for (int i = 0; i < cursorLength; i++) {
            int questionId = cursor.getInt(cursor.getColumnIndex(QuestionEntry._ID));
            String type = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TYPE));
            String question = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_QUESTION));
            String false1 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE1));
            String false2 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE2));
            String false3 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE3));
            String right = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_RIGHTANSWER));
            String taskRight = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TASKRIGHT));
            String taskWrong = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TASKWRONG));

            questions.add(new Question(questionId, 1, type, question, false1, false2, false3, right, taskRight, taskWrong));

            cursor.moveToNext();
        }

        //closing all connections
        cursor.close();
        db.close();

        return questions;
    }

    public ArrayList<Question> readQuestionsByType(String typeSearched) {
        //only getting active Questions
        String selection = QuestionEntry.COLUMN_NAME_TYPE + " = ?";

        // Specify arguments in placeholder order.
        String[] selectionArgs = {typeSearched};

        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {QuestionEntry._ID, QuestionEntry.COLUMN_NAME_ACTIVE, QuestionEntry.COLUMN_NAME_TYPE, QuestionEntry.COLUMN_NAME_QUESTION, QuestionEntry.COLUMN_NAME_FALSE1, QuestionEntry.COLUMN_NAME_FALSE2, QuestionEntry.COLUMN_NAME_FALSE3, QuestionEntry.COLUMN_NAME_RIGHTANSWER, QuestionEntry.COLUMN_NAME_TASKRIGHT, QuestionEntry.COLUMN_NAME_TASKWRONG};
        Cursor cursor = db.query(QuestionEntry.TABLE_NAME_QUESTION, // The table to query
                projection, // The columns to return
                selection, // The columns for the WHERE clause
                selectionArgs, // The values for the WHERE clause
                null, // don't group the rows
                null, // don't filter by row groups
                QuestionEntry._ID // No sorting required for only one entry
        );
        cursor.moveToFirst();
        int cursorLength = cursor.getCount();
        //in case there are no Players
        if (cursorLength == 0) {
            return null;
        }

        cursor.moveToFirst();

        ArrayList<Question> questions = new ArrayList<>();

        for (int i = 0; i < cursorLength; i++) {
            int questionId = cursor.getInt(cursor.getColumnIndex(QuestionEntry._ID));
            int active = cursor.getInt(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_ACTIVE));
            String type = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TYPE));
            String question = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_QUESTION));
            String false1 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE1));
            String false2 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE2));
            String false3 = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_FALSE3));
            String right = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_RIGHTANSWER));
            String taskRight = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TASKRIGHT));
            String taskWrong = cursor.getString(cursor.getColumnIndex(QuestionEntry.COLUMN_NAME_TASKWRONG));

            questions.add(new Question(questionId, active, type, question, false1, false2, false3, right, taskRight, taskWrong));

            cursor.moveToNext();
        }

        //closing all connections
        cursor.close();
        db.close();

        return questions;
    }

    public Player[] readPlayers() {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {PlayerEntry._ID, PlayerEntry.COLUMN_NAME_NAME, PlayerEntry.COLUMN_NAME_ACTIVE, PlayerEntry.COLUMN_NAME_ORDERNUMBER};
        Cursor cursor = db.query(PlayerEntry.TABLE_NAME_PLAYER, // The table to query
                projection, // The columns to return
                null, // The columns for the WHERE clause
                null, // The values for the WHERE clause
                null, // don't group the rows
                null, // don't filter by row groups
                PlayerEntry.COLUMN_NAME_ORDERNUMBER
        );

        int cursorLength = cursor.getCount();
        //in case there are no Players
        if (cursorLength == 0) {
            return null;
        }

        cursor.moveToFirst();

        Player[] players = new Player[cursorLength];

        for (int i = 0; i < cursorLength; i++) {
            int playerId = cursor.getInt(cursor.getColumnIndex(PlayerEntry._ID));
            String name = cursor.getString(cursor.getColumnIndex(PlayerEntry.COLUMN_NAME_NAME));
            int activeInt = cursor.getInt(cursor.getColumnIndex(PlayerEntry.COLUMN_NAME_ACTIVE));
            int orderNumber = cursor.getInt(cursor.getColumnIndex(PlayerEntry.COLUMN_NAME_ORDERNUMBER));

            boolean active = false;
            if (activeInt == 1) {
                active = true;
            }

            players[i] = new Player(playerId, name, active, orderNumber);

            cursor.moveToNext();
        }


        //closing all connections
        cursor.close();
        db.close();

        return players;
    }

    public int readActivePlayerId() {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {ActiveGameDataEntry.COLUMN_NAME_PLAYERID};
        Cursor cursor = db.query(ActiveGameDataEntry.TABLE_NAME_ACTIVEGAMEDATA, // The table to query
                projection, // The columns to return
                null, // The columns for the WHERE clause
                null, // The values for the WHERE clause
                null, // don't group the rows
                null, // don't filter by row groups
                null
        );

        int cursorLength = cursor.getCount();

        //in case there are no Players
        if (cursorLength == 0) {
            return 0;
        } else {

            cursor.moveToFirst();


            int playerId = cursor.getInt(cursor.getColumnIndex(ActiveGameDataEntry.COLUMN_NAME_PLAYERID));


            //closing all connections
            cursor.close();
            db.close();

            return playerId;
        }
    }

    public int readGameSize() {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {ActiveGameDataEntry.COLUMN_NAME_GAMESIZE};
        Cursor cursor = db.query(ActiveGameDataEntry.TABLE_NAME_ACTIVEGAMEDATA, // The table to query
                projection, // The columns to return
                null, // The columns for the WHERE clause
                null, // The values for the WHERE clause
                null, // don't group the rows
                null, // don't filter by row groups
                null
        );

        int cursorLength = cursor.getCount();

        //in case there are no Players
        if (cursorLength == 0) {
            return 0;
        } else {

            cursor.moveToFirst();


            int gameSize = cursor.getInt(cursor.getColumnIndex(ActiveGameDataEntry.COLUMN_NAME_GAMESIZE));


            //closing all connections
            cursor.close();
            db.close();

            return gameSize;
        }
    }

    /**
     * Remove an entry from the database.
     *
     * @param activeQuestionId the id of the entry to delete
     */
    public void removeActiveQuestion(int activeQuestionId) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Define 'where' part of query
        String selection = ActiveGameEntry.COLUMN_NAME_QUESTIONID + " = ?";

        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(activeQuestionId)};

        // Issue SQL statement.
        db.delete(ActiveGameEntry.TABLE_NAME_ACTIVEGAME, selection, selectionArgs);
    }

    /**
     * Remove an entry from the database.
     *
     * @param playerId the id of the entry to delete
     */
    public void removePlayer(int playerId) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Define 'where' part of query
        String selection = PlayerEntry._ID + " = ?";

        // Specify arguments in placeholder order.
        String[] selectionArgs = {String.valueOf(playerId)};

        // Issue SQL statement.
        db.delete(PlayerEntry.TABLE_NAME_PLAYER, selection, selectionArgs);
    }

    public Player[] switchPlayerUp(Player[] players, int toChangeOrderNumber) {
        SQLiteDatabase db = this.getWritableDatabase();

        int playersLength = players.length;

        //going through all players and looking for the selected Player to change the order
        for (int i = 0; i < playersLength; i++) {
            //looking for the right orderNumber
            if (players[i].getOrderNumber() == toChangeOrderNumber && i != 0) {
                int helpNumberOrder = players[i].getOrderNumber();
                int helpNumberPosition = i;
                helpNumberPosition--;
                //switching the OrderNumber one Up
                players[i].setOrderNumber(players[helpNumberPosition].getOrderNumber());
                players[helpNumberPosition].setOrderNumber(helpNumberOrder);
                //switching the Players so the Order fits to the Order Number
                Player helpPlayer = players[i];
                players[i] = players[helpNumberPosition];
                players[helpNumberPosition] = helpPlayer;
            }
        }

        //updating the order Numbers in the Database
        String selection = PlayerEntry._ID + " = ?";
        for (Player player : players) {
            // Specify arguments in placeholder order.
            String[] selectionArgs = {Integer.toString(player.getPlayerId())};
            ContentValues values = new ContentValues();
            values.put(PlayerEntry.COLUMN_NAME_ORDERNUMBER, player.getOrderNumber());

            db.update(PlayerEntry.TABLE_NAME_PLAYER, values, selection, selectionArgs);
        }
        //closing all connections
        db.close();
        return players;
    }

    public Player[] switchPlayerDown(Player[] players, int toChangeOrderNumber) {
        SQLiteDatabase db = this.getWritableDatabase();

        int playersLength = players.length;

        //going through all players and looking for the selected Player to change the order
        for (int i = 0; i < playersLength; i++) {
            //looking for the right orderNumber
            if (players[i].getOrderNumber() == toChangeOrderNumber && i != playersLength - 1) {
                int helpNumberOrder = players[i].getOrderNumber();
                int helpNumberPosition = i;
                helpNumberPosition++;
                //switching the OrderNumber one Down
                players[i].setOrderNumber(players[helpNumberPosition].getOrderNumber());
                players[helpNumberPosition].setOrderNumber(helpNumberOrder);
                //switching the Players so the Order fits to the Order Number
                Player helpPlayer = players[i];
                players[i] = players[helpNumberPosition];
                players[helpNumberPosition] = helpPlayer;
            }
        }

        //updating the order Numbers in the Database
        String selection = PlayerEntry._ID + " = ?";
        for (Player player : players) {
            // Specify arguments in placeholder order.
            String[] selectionArgs = {Integer.toString(player.getPlayerId())};
            ContentValues values = new ContentValues();
            values.put(PlayerEntry.COLUMN_NAME_ORDERNUMBER, player.getOrderNumber());

            db.update(PlayerEntry.TABLE_NAME_PLAYER, values, selection, selectionArgs);
        }
        //closing all connections
        db.close();
        return players;
    }

    public void changeQuestionsActive(int questionId, int activeStatus) {
        SQLiteDatabase db = this.getWritableDatabase();

        //updating the order Numbers in the Database
        String selection = QuestionEntry._ID + " = ?";

        String[] selectionArgs = {Integer.toString(questionId)};

        ContentValues values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, activeStatus);

        db.update(QuestionEntry.TABLE_NAME_QUESTION, values, selection, selectionArgs);
        //closing all connections
        db.close();
    }


    private void initialiseQuestions(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Welches Land konsumiert am meisten Alkohol?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "Polen");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "Deutschland");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "Tschechien");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "Weißrussland");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Oh Blyat \uD83C\uDF7A, 17,6 Liter reinen Alkohol pro Jahr! Du darfst 4 Schlücke verteilen!");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Leider falsch! Mach's wie die Weißrussen und trink 3 Schlücke! \uD83C\uDF7A");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Welche Biermarke \uD83C\uDF7A wurde 2015 am häufigsten verkauft?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "Augustiner");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "Oettinger");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "Krombacher");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "Radeberger");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Ein echter Bierkenner! \uD83C\uDF7B Du darfst 3 Schlücke verteilen!");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Aiaiai nimm erstmal ein paar Schlücke.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Welcher Präsident hat die meisten Wahlversprechen eingehalten?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "John F. Kennedy");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "George W. Bush Jr.");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "Barrack Obama");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "Donald Trump");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Make America Great Again! \uD83C\uDDFA\uD83C\uDDF8 Und verteile 5 Schlücke");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Blöde Frage! Dumme Antwort! Trink erstmal 2!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Was ist in der Schweiz absolut illegal?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "Kannabis rauchen");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "Nach 21 Uhr Jodeln");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "Hunde essen");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "1 Lama zu halten");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Da wir alle soziale Wesen, wie die Lamas, sind stoßen alle an! \uD83C\uDF7B");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Da wir alle soziale Wesen, wie die Lamas, sind stoßen alle an! \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Vervollständige: ''Fahrrad fahren, wenig Fleisch und vor allem...''");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "Umweltschutz");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "wenig Verpackung");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "Recyclen");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "Dousenbiersaufen");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Wir machen mit bei der Rettung der Erde! Trennt das flüssige vom Glas!");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Für diese massive Wissenslücke gibt es satte \n5 Strafschlücke! \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Was ist der Weltrekord im 1 Liter Bier Exen? \uD83C\uDF7A");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "1 Sekunde");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "1,7 Sekunden");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "2,5 Sekunden");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "1,3 Sekunden");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Natürlich sind es 1,3 Sekunden, Clever! Wähle jemanden der es schneller machen soll!");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Leider nein, wenn dein Glas mehr als halb leer ist, versuchs schneller als 1,3 Sekunden");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Was ist das am häufigsten besuchte Land?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "USA");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "England");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "Italien");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "Frankreich");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Oui, Oui, alle die Wein trinken, stoßen an! 3 Schlücke!. \uD83C\uDF77");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Leider nicht! 2 Schlücke für dich selbst.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Welches Land benutzt das metrische System?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "Myanmar");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "USA");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "Liberia");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "Tuvalu");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Jeder der Tuvalu nicht kannte muss trinken!");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Du trinkst pro 10000 Einwohner von Tuvalu 1 Schluck, als 1. \uD83C\uDF7A");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Was ist der Weltrekord im volle Maßkrüge 40m tragen? \uD83C\uDF7B");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "56");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "34");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "22");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "29");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Richtig! Du darfst eine Trinkregel aufstellen!");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Falsch! Du machst erstmal dein Glas etwas leichter! 3 Schlücke! \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "In welchem Jahr wurde der Besucherrekord der Wiesen aufgestellt?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "1996");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "2017");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "2008");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "1985");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "7,1 Millionen waren es damals! Verteile 5 Schlücke! \uD83C\uDF7B\uD83C\uDF7B");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Die Gruppe darf bei dir eine Instagram/Snapchat Story machen oder du musst Exen!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wann war die deutsche Wiedervereinigung?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "4.11.1989");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "4.9.1990");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "4.7.1991");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "3.10.1990");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Alles andere wäre ja auch peinlich gewesen!");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Wow super Leistung! Lass dir 3.10.1990 ins Gesicht schreiben!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);


        //questionPlayer category
        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Welcher Spieler ist am arrogantesten?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Da ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " so arrogant ist, muss er/sie erstmal 3 Schlücke trinken! \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Welchen der Spieler kennst du am längsten?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Du und ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " seid jetzt Trinkkumpel, ihr trinkt immer gemeinsam!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wen findest du am verrücktesten?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " muss ein Tier deiner Wahl imitieren! Dann darf er/sie 5 verteilen.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer würde am ehesten einen Mord begehen?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " Mord ist dein Hobby! Wähle jemanden der immer trinken muss wenn du trinkst.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer soll einfach etwas mehr saufen?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " sauf doch dann mal 5 Schlücke! \uD83C\uDF7B\uD83E\uDD42");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer ist am geizigsten?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Geiz ist geil! ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " darf eine Trinkregel aufstellen.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer ist aktuell am betrunkensten?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Alle außer ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " müssen trinken!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer ist der beste Fußballspieler?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Für jedes Tor das ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " diese Saison gemacht hat darf er/sie einen verteilen!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wem kann man am meisten vertrauen?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Da wir alle ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " vertrauen darf er/sie 6 Schlücke verteilen! \uD83C\uDF7B\uD83C\uDF7B\uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer ist am schlechtesten in Mathe?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Aufgehts ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " du darfst 14-6x2 Schlücke verteilen. \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer ist am ordentlichsten?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " muss den Tisch etwas säubern. (Leere Gläser/Flaschen aufräumen)");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);


        //task category
        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Der Spieler der zuerst ein Elternteil ans Telefon bekommt darf 10 Schlücke verteilen!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Der kleinste Spieler darf 3 Schlücke verteilen. \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer zuletzt etwas gegessen hat, hat eine gute Unterlage und muss 5 Schlücke trinken. \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Trink so viele Schlücke, wie du Vokale im Vornamen hast.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "How dare you?! Alle die im letzten Jahr geflogen sind müssen 5 Schlücke trinken.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Riese und Zwerg. Der größte und kleinste Spieler dürfen 3 Schlücke verteilen!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Zeig deine letzten 5 Fotos der Gruppe oder trink 5 Schlücke.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Lies deine letzte Nachricht laut vor oder trink 3 Schlücke!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Kategorie! Derjenige der nichts mehr weiß/etwas doppelt sagt muss 5 trinken.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Trinkregel! Du darfst eine Trinkregel aufstellen. Falls sie jemand vergisst gibts 2 Strafschluck!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Alle die ein schwarzes Oberteil tragen müssen 2 Schlücke trinken. \uD83E\uDD42");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Spielt eine Runde Wahrheit oder Pflicht. Jeder der sich weigert 3 Schlücke!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Spielt der Reihe nach eine Runde ''Ich hab noch nie..'' jeder der es schon gemacht hat muss einen Schluck trinken!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Du spielst ''Ich sehe was, was du nicht siehst'' gegen die Gruppe. Der Sieger darf 4 verteilen. ");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Alle gratulieren dem Spieler der zuletzt Geburtstag hatte. \uD83C\uDF89\uD83C\uDF8A");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Alle die dieses Jahr schon Geburtstag hatten müssen 3 Schlücke trinken! \uD83C\uDF89");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Der Spieler mit den kürzesten/wenigsten Haaren darf 5 verteilen!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);


        //BVWD FRAGEN
        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer ist am hässlichsten?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "Thomas Brunner");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "Thomas Brunner");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "Thomas Brunner");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "Thomas Brunner");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Für diese extreme Leistung trinkt Thomas Brunner erstmal 5 Schlücke!");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Für diese extreme Leistung trinkt Thomas Brunner erstmal 5 Schlücke!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer musste sich beim letzen Frühshoppen in Regensburg City als erstes übergeben?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "CL aus P");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "KT aus A");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "JF aus E");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "FH aus F");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Da mich Florian Hödl so stolz macht, darf er 8 verteilen!");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Falsch! Du nimmst erstmal 3 Schlücke! \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer bekommt im Schnitt die meisten gelben Karten pro Spiel?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "Heisse");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "Fisch");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "Wookmäään");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "Schrecki");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Fairplay Fehlanzeige! Auf 113 Spiele 35 Gelbe! Schrecki trinkt 5!");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Trink für jede gelbe Karte von Heisse einen Schluck also 5!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wie oft hat Christian Leitermann in Novalja gekotzt?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "1");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "6");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "3");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "2");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Richtig, du darf mit dem Bierkönig aus Novalja 3 Schlücke trinken.");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Blödmann! Der Bierkönig aus Novalja kotzt doch nicht so oft! 3 Schlücke.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer ist der leichteste Gegner?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Du und ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " spielt eine Runde Chicago, der Verlierer muss 5 Schlücke\uD83C\uDF7B trinken.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);
    }

    private void initialiseQuestionsPerverted(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Was ist der Weltrekord für unterschiedliche Sexpartner an einem Tag?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "276");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "420");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "69");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "919");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Richtige Gönnung! Du darfst 919 Schlücke verteilen, wenn du mehr hattet.");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Leider war es etwas mehr als das, trink für jeden Mitspieler einen Schluck!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wie oft hattest du dieses Jahr Sex?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "nie");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "1-3");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "4-5");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "über 5");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Respekt du Angeber, dann trink mal über 5 Schlücke selbst.");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Das ist aber schade, du darfst aus Mitleid 3 verteilen. \uD83E\uDD42");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wie viele Menschen nahmen an der größten Orgie der Welt teil?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "1198");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "876");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "475");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "500");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "In Japan gehts verrückt zu! Verteile für jeden Anime den du kennst einen Schluck.");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Leider falsch! Trink für jede Person mit der du Sex hattest einen Schluck!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer riecht am besten?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " verrate uns dein Geheimnis! Du musst eine Frage der Gruppe beantworten.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wen findest du am attraktivsten?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " bedankt sich und darf 5 Schlücke verteilen! \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wesen Oberteil gefällt dir am besten?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Du musst dein Oberteil mit ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " tauschen! \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wen kennst du am wenigsten/kürzesten?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Du und ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " kommt euch etwas näher und trinkt beide 3 Schlücke! \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Der jenige, der zuletzt Sex hatte darf 5 Schlücke verteilen!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Jeder gibt der Person rechts von einem einen Kuss auf die Wange.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);
    }

    private void initialiseQuestionsGuysRound(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 2);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer ist der schlechteste Fußballspieler?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "Lionel Messi");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "Antoine Griezmann");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "Luka Modrić");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "Cristiano Ronaldo");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Dieses Trinkspiel mag einfach Ronaldo nicht! 5 Schlücke an jeden Fan.");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Dieses Trinkspiel mag einfach Ronaldo nicht! 5 Schlücke an jeden Fan.");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "question");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 2);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wie weit war die weiteste Ejakulation \uD83D\uDCA7☔ der Welt?");
        values.put(QuestionEntry.COLUMN_NAME_FALSE1, "2,56 m");
        values.put(QuestionEntry.COLUMN_NAME_FALSE2, "8,13 m");
        values.put(QuestionEntry.COLUMN_NAME_FALSE3, "4,72 m");
        values.put(QuestionEntry.COLUMN_NAME_RIGHTANSWER, "5,76 m");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Da kennt sich jemand mit Spritzen aus! 3 Schlücke verteilen.");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, "Trink deine weiteste Ejakulation in Schlücken! \n(pro 20cm)");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 2);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Mit wem würdest du am ehesten einen Dreier haben wollen?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Du und ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " trinkt euch Mut dafür an 3 Schlücke. \uD83C\uDF7B");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 2);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer kann am besten Saufen?! \uD83C\uDF7B\uD83C\uDF7B\uD83C\uDF7B");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "Na dann! ");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " zeig uns was du kannst! 5 Schlücke!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "questionPlayers");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 0);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Wer ist am unfreundlichsten zu Fremden?");
        values.put(QuestionEntry.COLUMN_NAME_TASKRIGHT, "");
        values.put(QuestionEntry.COLUMN_NAME_TASKWRONG, " du bist das Arschloch! Alle dürfen dich beleidigen!");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 2);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Alle richtigen Saufautomaten zeigen was sie können. \uD83E\uDD42");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);

        values = new ContentValues();
        values.put(QuestionEntry.COLUMN_NAME_TYPE, "task");
        values.put(QuestionEntry.COLUMN_NAME_MODE, 2);
        values.put(QuestionEntry.COLUMN_NAME_ACTIVE, 1);
        values.put(QuestionEntry.COLUMN_NAME_ASKED, 0);
        values.put(QuestionEntry.COLUMN_NAME_QUESTION, "Jeder Spieler trinkt seine Penislänge in Schluck! \n(3cm = 1 Schluck)");
        db.insert(QuestionEntry.TABLE_NAME_QUESTION, "", values);
    }
}
