package com.drinkinggame.listAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.drinkinggame.R;
import com.drinkinggame.dataBase.DrinkingGameLocalDatabaseSqlite;
import com.drinkinggame.dataBase.dataObjects.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class PlayerListAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<Player> listPlayer;
    private Context context;
    private DrinkingGameLocalDatabaseSqlite databaseSqlite;



    public PlayerListAdapter(ArrayList<Player> listPlayer, Context context) {
        this.listPlayer = listPlayer;
        this.context = context;
        databaseSqlite = new DrinkingGameLocalDatabaseSqlite(context);
    }

    @Override
    public int getCount() {
        return listPlayer.size();
    }

    @Override
    public Object getItem(int pos) {
        return listPlayer.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        //return list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custome_list_item, null);
        }

        //Handle TextView and display string from your list
        TextView listItemText = view.findViewById(R.id.playerName);
        listItemText.setText(listPlayer.get(position).getName());

        //Handle buttons and add onClickListeners
        Button deleteBtn = view.findViewById(R.id.deleteButton);
        Button upButton = view.findViewById(R.id.upButton);
        Button downButton = view.findViewById(R.id.downButton);


        deleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                databaseSqlite.removePlayer(listPlayer.get(position).getPlayerId());
                listPlayer.remove(position);
                notifyDataSetChanged();
            }
        });

        upButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Player[] playersListArray = listPlayer.toArray(new Player[0]);
                Player[] players = databaseSqlite.switchPlayerUp(playersListArray, listPlayer.get(position).getOrderNumber());
                if (players != null) {
                    listPlayer = new ArrayList<>(Arrays.asList(players));
                    notifyDataSetChanged();
                }
            }
        });

        downButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Player[] playersListArray = listPlayer.toArray(new Player[0]);
                Player[] players = databaseSqlite.switchPlayerDown(playersListArray, listPlayer.get(position).getOrderNumber());
                if (players != null) {
                    listPlayer = new ArrayList<>(Arrays.asList(players));
                    notifyDataSetChanged();
                }
            }
        });

        return view;
    }
}