package com.drinkinggame.listAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.drinkinggame.R;
import com.drinkinggame.dataBase.DrinkingGameLocalDatabaseSqlite;
import com.drinkinggame.dataBase.dataObjects.Question;

import java.util.ArrayList;

public class QuestionListAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<Question> listQuestion;
    private Context context;
    private DrinkingGameLocalDatabaseSqlite databaseSqlite;


    public QuestionListAdapter(ArrayList<Question> listQuestion, Context context) {
        this.listQuestion = listQuestion;
        this.context = context;
        databaseSqlite = new DrinkingGameLocalDatabaseSqlite(context);
    }

    @Override
    public int getCount() {
        return listQuestion.size();
    }

    @Override
    public Object getItem(int pos) {
        return listQuestion.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        //return list.get(pos).getId();
        //just return 0 if your list items do not have an Id variable.
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custome_list_item_rules, null);
        }

        //Handle TextView and display string from your list
        TextView listItemText = view.findViewById(R.id.questionTextRules);
        listItemText.setText(listQuestion.get(position).getQuestion());

        CheckBox checkBox = view.findViewById(R.id.checkboxRules);
        RelativeLayout listItemRules = view.findViewById(R.id.listItemRules);
        if (listQuestion.get(position).getActive() == 1) {
            listItemRules.setBackgroundResource(R.drawable.rounded_button);
            checkBox.setChecked(true);
        } else {
            listItemRules.setBackgroundResource(R.drawable.rounded_grey_button);
            checkBox.setChecked(false);
        }

        checkBox.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                CheckBox checkBox = v.findViewById(v.getId());
                if (checkBox.isChecked()) {
                    databaseSqlite.changeQuestionsActive(listQuestion.get(position).getQuestionId(), 1);
                    listQuestion.get(position).setActive(1);
                    notifyDataSetChanged();
                } else {
                    databaseSqlite.changeQuestionsActive(listQuestion.get(position).getQuestionId(), 0);
                    listQuestion.get(position).setActive(0);
                    notifyDataSetChanged();
                }
            }
        });

        return view;
    }
}